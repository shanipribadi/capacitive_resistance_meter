#include "resistor.h"

void setup() {
    Serial.begin(115200);
    Serial.println(F("Begin"));
}

void loop() {
    uint16_t resistor;
    uint16_t us, ns;

    while (1) {
	resistor = measure_res(0, 0);
	Serial.print(F("RC: "));
	Serial.print(resistor);
	Serial.println(F(" (* 0.25 us)"));
	Serial.print(F("R: "));
	Serial.print(((resistor * 119) >> 6) + 28);
	Serial.println(F(" (ohm)"));

	measure_res(&us, &ns);
	Serial.print(F("us: "));
	Serial.print(us);
	Serial.print(F("."));
	Serial.println(ns);
	delay(500);
    }
}
