#include <avr/io.h>
#include <util/delay.h>

#include "resistor.h"

#define DDR_DIN DDRD
#define PORT_DIN PORTD
#define PIN_DIN PIND
#define P_DIN PD6

uint16_t measure_res(uint16_t *us, uint16_t *ns) {
    uint16_t t_time = 0;
    uint8_t t_count = 7 * 16; // oversample
    uint8_t t;

    while(t_count--) {
	t = 1;
	// discharge
	DDR_DIN |= _BV(P_DIN);
	PORT_DIN &= ~_BV(P_DIN);
	_delay_us(50); //50 us discharge cycle
	// tri state
	DDR_DIN &= ~_BV(P_DIN);
	/*PORT_DIN &= ~_BV(P_DIN);*/

	while(!(PIN_DIN & _BV(P_DIN)) && t) {
	    t++; //each loop is 7 cycle long
	}
	t_time += t;
    }
    if (t_time) {
	t_time -= 7 * 16;
    }

    if (us) {
	*us = t_time >> 8;
    }
    if (ns) {
	*ns = (t_time % 256) * 4;
    }

    return t_time >> 6; // downsample
}
