### DISCLAIMER
### This is an example Makefile and it MUST be configured to suit your needs.
### For detailled explanations about all the avalaible options,
### please refer to https://github.com/sudar/Arduino-Makefile/blob/master/arduino-mk-vars.md

ARDUINO_LIBS = SoftwareSerial
### BOARD_TAG
### It must be set to the board you are currently using. (i.e uno, mega2560, etc.)
BOARD_TAG         = nano
BOARD_SUB = atmega328

### MONITOR_BAUDRATE
### It must be set to Serial baudrate value you are using.
#MONITOR_BAUDRATE  = 57600

### MONITOR_PORT
### The port your board is connected to. Using an '*' tries all the ports and finds the right one.
MONITOR_PORT      = /dev/ttyUSB*

ISP_PROG = avrisp
ISP_PORT = /dev/ttyUSB*
AVRDUDE_ISP_BAUDRATE = 19200
AVRDUDE_OPTS = -v

### PROJECT_DIR
### This is the path to where you have created/cloned your project
PROJECT_DIR       = $(shell echo $(HOME))/sketchbook

### USER_LIB_PATH
### Path to where the your project's libraries are stored.
#USER_LIB_PATH     :=  $(PROJECT_DIR)/libraries

### ARDMK_DIR
### Path to the Arduino-Makefile directory.
ARDMK_DIR         = /usr/share/arduino

### ARDUINO_DIR
### Path to the Arduino application and ressources directory.
#ARDUINO_DIR       = /usr/share/arduino

### AVR_TOOLS_DIR
### Path to the AVR tools directory such as avr-gcc, avr-g++, etc.
AVR_TOOLS_DIR     = /usr
CPPFLAGS = -fno-use-cxa-atexit -g

### AVRDUDE
### Path to avrdude directory.
#AVRDUDE          = /usr/bin/avrdude

### CPPFLAGS
### Flags you might want to set for debugging purpose. Comment to stop.
#CPPFLAGS         = -pedantic -Wall -Wextra

### don't touch this
CURRENT_DIR       = $(shell basename $(CURDIR))

### OBJDIR
### This is were you put the binaries you just compile using 'make'
#OBJDIR            = $(PROJECT_DIR)/bin/$(BOARD_TAG)/$(CURRENT_DIR)

### path to Arduino.mk, inside the ARDMK_DIR, don't touch.
include $(ARDMK_DIR)/Arduino.mk

TARGET_AXF = $(OBJDIR)/$(TARGET).axf

$(TARGET_AXF): $(LOCAL_OBJS) $(CORE_LIB) $(OTHER_OBJS)
	$(CC) $(LDFLAGS) -Wl,--undefined=_mmcu,--section-start=.mmcu=0x91000000 \
	    -o $@ $(LOCAL_OBJS) $(CORE_LIB) $(OTHER_OBJS) -lm

axf: $(TARGET_AXF)
