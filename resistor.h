#ifndef RESISTOR_H_
#define RESISTOR_H_
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

uint16_t measure_res(uint16_t *us, uint16_t *ns);

#ifdef __cplusplus
}
#endif
#endif

